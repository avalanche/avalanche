import urllib2
import gtk
from random import randrange
from ast import literal_eval

clipboard = gtk.clipboard_get()

n_beers = 25
beers = [randrange(0, 5000) for i in xrange(n_beers)]
beer_data = []

print 'Grabbing %s random beers...' % (n_beers)

for i in beers:
    handler = urllib2.urlopen('http://localhost:8000/beers/api/v1/beer/%s/?format=json' % (i))
    beer_data.append(literal_eval(handler.read()))
    handler.close()

clipboard.set_text(str(beer_data))
clipboard.store()

print 'Done. Results saved to clipboard'
