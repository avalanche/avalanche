init:
	pip install -r requirements.txt

test:
	nosetests -v tests/* src/*

doc: 
	make -C docs html

static:
	python manage.py collectstatic --noinput

reset:
	make clean
	make alldata
	make static

data:
	python manage.py syncdb --noinput
	python manage.py migrate --noinput

alldata:
	make data
	make store
	make wb

store:
	python manage.py loaddata breweries
	python manage.py loaddata categories
	python manage.py loaddata styles
	python manage.py loaddata beers
	python manage.py loaddata wines
	python manage.py loaddata orders
	
wb:
	#careful order matters here!
	python manage.py loaddata work
	python manage.py loaddata datasets	
	python manage.py loaddata results

clean:
	rm -rf site_media/static/*
	rm -rf prod.sqlite
	
run:
	python manage.py runserver

.PHONY : clean
.PHONY : static
