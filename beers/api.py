__author__ = 'hur1can3'

from tastypie.resources import ModelResource
from tastypie.serializers import Serializer
from beers.models import Beer, Brewery, Style


class StyleResource(ModelResource):
    class Meta:
        queryset = Style.objects.all()
        resource_name = 'style'
        #excludes = ['created', 'modified', 'slug']
        serializer = Serializer()
        include_resource_uri = False
        allowed_methods = ['get', 'post']


class BreweryResource(ModelResource):
    class Meta:
        queryset = Brewery.objects.all()
        resource_name = 'brewery'
        #excludes = ['created', 'modified', 'slug']
        include_resource_uri = False
        serializer = Serializer()
        allowed_methods = ['get', 'post']


class BeerResource(ModelResource):
    class Meta:
        queryset = Beer.objects.all()
        resource_name = 'beer'
        #excludes = ['blockmap', 'created', 'modified', 'slug']
        include_resource_uri = False
        serializer = Serializer()
        allowed_methods = ['get', 'post']
