from datetime import datetime
from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField

# Create your models here.


class Category(TimeStampedModel):
    name = models.CharField(max_length=200)
    last_mod = models.DateTimeField()
    slug = AutoSlugField(populate_from='name',
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "categories"

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        self.last_mod = datetime.now()
        super(Category, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('category_edit', kwargs={'pk': self.pk})


class Style(TimeStampedModel):
    last_mod = models.DateTimeField()
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name',
                         unique_with=('category__name'),
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        self.last_mod = datetime.now()
        super(Style, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('style_edit', kwargs={'pk': self.pk})


class Brewery(TimeStampedModel):
    website = models.CharField(max_length=200, blank=True)
    city = models.CharField(max_length=200, blank=True)
    last_mod = models.DateTimeField()
    name = models.CharField(max_length=200)
    address1 = models.CharField(max_length=200, blank=True)
    address2 = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    phone = models.CharField(max_length=30, blank=True)
    state = models.CharField(max_length=200, blank=True)
    country = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from='name',
                         unique_with=('country'),
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "breweries"

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        self.last_mod = datetime.now()
        super(Brewery, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('brewery_edit', kwargs={'pk': self.pk})


class Beer(TimeStampedModel):
    name = models.CharField(max_length=200)
    last_mod = models.DateTimeField()
    brewery = models.ForeignKey(Brewery)
    style = models.ForeignKey(Style)
    description = models.TextField()
    abv = models.FloatField(default=5.00)
    picture = models.ImageField(upload_to="beers", default="generic.png", blank=True)
    price = models.FloatField(default=0.00, blank=True)
    onstock = models.PositiveSmallIntegerField(default=1, blank=True)
    purchased = models.PositiveSmallIntegerField(default=0, blank=True)
    slug = AutoSlugField(populate_from='name',
                         unique_with=('brewery__name'),
                         slugify=lambda value: value.replace(' ', '-'))
    # rating = models.FloatField()

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        self.last_mod = datetime.now()
        super(Beer, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('beer_edit', kwargs={'pk': self.pk})


class Store(TimeStampedModel):
    name = models.CharField(max_length=300)
    beers = models.ManyToManyField(Beer)

    # TODO: Defne custom methods here
