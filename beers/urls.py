from django.conf import settings
from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from tastypie.api import Api
from beers.views import CategoryListView, BeerEdit, BeerListView, BeerDelete
from beers.api import StyleResource, BreweryResource, BeerResource

#setup api resources
v1_api = Api(api_name='v1')
v1_api.register(StyleResource())
v1_api.register(BreweryResource())
v1_api.register(BeerResource())

SLUG = '(?P<slug_id>[\w\d-]+)'
STYLE_SLUG = '(?P<style_slug_id>[\w\d-]+)'
BEER_SLUG = '(?P<beer_slug>[\w\d-]+)'

urlpatterns = patterns('beers.views',
    url(r'^api/', include(v1_api.urls)),
    url(r'^$', CategoryListView.as_view(), name="beer_home"),
    url(r'^' + SLUG + '/$', 'category_style', name="beer_styles"),
    url(r'^' + SLUG + '/' + STYLE_SLUG + '/$', BeerListView.as_view(), name='beer_list'),
    url(r'^' + SLUG + '/' + STYLE_SLUG + '/' + BEER_SLUG + '/$', BeerEdit.as_view(), name='beer_edit'),
    url(r'^' + SLUG + '/' + STYLE_SLUG + '/' + BEER_SLUG + '/delete/$', BeerDelete.as_view(), name='beer_delete'),
    url(r'^' + SLUG + '/' + STYLE_SLUG + '/page/(?P<page>\d+)/$', BeerListView.as_view(), name='beer_index_paginated')
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()
