from django.views.generic import ListView, DeleteView, TemplateView
from django.views.generic.list_detail import object_list
from django.core.urlresolvers import reverse_lazy

from beers.models import Beer, Category, Style


class BeerListView(ListView):
    model = Beer
    paginate_by = 6
    allow_empty = True
    template_name = "beers/beer_list.html"

    def get_queryset(self):
        style_slug_id = self.kwargs['style_slug_id']
        style_id = Style.objects.filter(slug=style_slug_id)
        return Beer.objects.filter(style=style_id)

    def get_context_data(self, **kwargs):
        style_slug_id = self.kwargs['style_slug_id']
        context = super(BeerListView, self).get_context_data(**kwargs)
        context['beer_list'] = Beer.objects.filter(slug=style_slug_id)
        return context


class CategoryListView(ListView):
    model = Category
    template_name = "beers/index.html"
    context_object_name = "beer_categories"


def category_style(request, slug_id):
    category_id = Category.objects.filter(slug=slug_id)
    queryset = Style.objects.filter(category=category_id)
    return object_list(request, queryset=queryset)


class BeerEdit(TemplateView):
    model = Beer
    template_name = "beers/detail.html"

    def get_context_data(self, **kwargs):
        context = super(BeerEdit, self).get_context_data(**kwargs)
        context['beer'] = Beer.objects.get(
            slug=self.kwargs.get('beer_slug', None))
        return context


class BeerDelete(DeleteView):
    model = Beer
    success_url = reverse_lazy('beer_index_paginated')
