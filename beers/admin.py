import random
import decimal

from beers.models import Beer, Brewery, Style, Category
from django.contrib import admin


def update_all(modeladmin, request, queryset):
    queryset.update(
        price=decimal.Decimal(random.randrange(500, 3000)) / 100)
    queryset.update(onstock=random.randrange(1, 30))
    queryset.update(purchased=random.randrange(1, 30))
    update_all.short_description = "Update default values with random values"


def resave(modeladmin, request, queryset):
    for b in queryset:
        if b.abv == 0:
            b.abv = decimal.Decimal(random.randrange(500, 1500)) / 100
        b.price = decimal.Decimal(random.randrange(500, 3000)) / 100
        b.onstock = random.randrange(1, 30)
        b.purchased = random.randrange(1, 30)
        b.picture = "beers/generic.png"
        b.save()


class BeerAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    ordering = ['slug']
    actions = [resave]

admin.site.register(Beer, BeerAdmin)
admin.site.register(Brewery)
admin.site.register(Category)
admin.site.register(Style)
