from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from wines.models import Wine


@dajaxice_register
def order(request, cid):
    dajax = Dajax()
    w = Wine.objects.get(pk=cid)
    out = []
    out.append("<li><a href='%s'>%s</a></li>" % (str(w.id), str(w.name)))
    #dajax.assign('#order_item','value',str(product))
    dajax.assign('#order_item', 'innerHTML', ''.join(out))
    return dajax.json()
