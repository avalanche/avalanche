from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField


class Wine(TimeStampedModel):
    name = models.CharField(max_length=200)
    year = models.SmallIntegerField(max_length=4)
    grapes = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    region = models.CharField(max_length=200)
    description = models.TextField()
    price = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    onstock = models.PositiveSmallIntegerField(default=1)
    purchased = models.PositiveSmallIntegerField(default=0)
    picture = models.ImageField(upload_to="wines")
    slug = AutoSlugField(populate_from='name',
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        super(Wine, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('wine_edit', kwargs={'pk': self.pk})

# TODO: Defne custom methods here
