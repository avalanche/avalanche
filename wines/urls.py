from django.conf import settings
from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from tastypie.api import Api

from wines.views import WineListView, WineEdit, WineDelete
from wines.api import WineResource

#setup api resources
v1_api = Api(api_name='v1')
v1_api.register(WineResource())


SLUG = '(?P<slug>[\w\d-]+)'

urlpatterns = patterns('wines.views',
                       url(r'^api/', include(v1_api.urls)),
                       url(r'^$', WineListView.as_view(), name="wine_home"),
                       url(r'^' + SLUG + '/$',
                           WineEdit.as_view(), name='wine_edit'),
                       url(r'^' + SLUG + '/delete/$',
                           WineDelete.as_view(), name='wine_delete'),
                       url(r'^page/(?P<page>\d+)/$', WineListView.as_view(
                       ), name='wine_index_paginated')
                       )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
