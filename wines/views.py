from django.views.generic import ListView, DeleteView, TemplateView
from django.core.urlresolvers import reverse_lazy

from wines.models import Wine


class WineListView(ListView):
    model = Wine
    paginate_by = 6
    allow_empty = True
    template_name = "wines/index.html"


class WineEdit(TemplateView):
    model = Wine
    template_name = "wines/detail.html"

    def get_context_data(self, **kwargs):
        context = super(WineEdit, self).get_context_data(**kwargs)
        context['wine'] = Wine.objects.get(slug=self.kwargs.get('slug', None))
        return context


class WineDelete(DeleteView):
    model = Wine
    success_url = reverse_lazy('wine_index_paginated')
