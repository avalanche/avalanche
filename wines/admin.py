from wines.models import Wine
from django.contrib import admin


class WineAdmin(admin.ModelAdmin):
    list_display = ('name', 'year')

admin.site.register(Wine)
