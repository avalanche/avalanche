__author__ = 'hur1can3'

from tastypie.resources import ModelResource
from tastypie.serializers import Serializer
from wines.models import Wine


class WineResource(ModelResource):
    class Meta:
        queryset = Wine.objects.all()
        resource_name = 'wine'
        #excludes = ['blockmap', 'created', 'modified', 'slug']
        include_resource_uri = False
        serializer = Serializer()
        allowed_methods = ['get', 'post']
