# Avalanche Django Project #

## About ##
***

Avalanche is a data mining and analytics framework with an added emphasis on data propogation, visual components and presentation. It provides a pluggable environment that allows for simple integration of scalable analytic components (or blocks), which can perform operations on ambiguous sets of data. This resulting data can then be used in a wide array of visual components that are integrated in the system, such as interactive graphs, tables, charts, etc. The modified data can also be propogated to other blocks for further processing.

The framework was implemented as a cloud service that can be accessed through an API or through a user interface, similar to Salesforce.com. This way users can programmatically work with their data or simply use our intuitive user interface. Regardless of the developers preference, achieving flexibility through simplicity will be our primary design goal. We want this system to be suitable for any type of data that can be analytically/statistically analyzed.



## Features ##
* Pluggable analytical components - Done
* Visual presentation modules (graphs, tables, forms, etc.)
* Data aggregations (e.g., average orders per day) - In progress
* Data clustering / classifications - Done
* Association learning - Done
* Rich graphing modules (using pluggable graphing libraries, d3.js)
* Predictions and estimates (using statistical analysis)
* Trend / Pattern recognition
* Queryable statistics (allow for queryable database) - In progress

## Technology Stack ##
***
### 'Backend'
* Python
* MongoDB - JavaScript based NoSQL database
* Celery - asynchronous distrubted task queue
* Scikit Learn - advanced machine learning python library
* Scipy and Numpy - highly optimized data computations in python
* Flask - python micro web framework, used to support RESTful URLS

### 'Frontend'
* Django - python based website framework
* Jquery - javascript ajax support
* Twitter Bootstrap Framework - simple css/js framework
* TastyPie - JSON based RESTful API for Django
* Gargoyle - Toggable Feature flips for administration interface - NOT IMPLEMENTED
* Experiements - A/B analysis for interface design - NOT IMPLEMENTED

## Prerequisites ##

* Python >= 2.5
* pip
* virtualenv (virtualenvwrapper is recommended for use during development)

these can be installed with:
'sudo apt-get build-essential python-pip python-virtualenv mercurial git-core'

## Installation ##
***

### Creating the environment ###

Create a virtual python environment for the project.

for virtualenvwrapper add this to your .bashrc file in the user home directory:
                
                export WORKON_HOME=$HOME/.virtualenvs
                export PROJECT_HOME=$HOME/project
                source /usr/local/bin/virtualenvwrapper.sh


If you're not using virtualenv or virtualenvwrapper you may skip this step.

#### For virtualenvwrapper (RECOMMENDED) ####

        mkvirtualenv --no-site-packages avalanche

#### For virtualenv ####

        virtualenv --no-site-packages avalanche
        source avalanche-env/bin/activate

### Clone the code (if already cloned skip to next step) ###

Obtain the url to your git repository.

        git clone <URL_TO_GIT_RESPOSITORY> avalanche

### Install requirements ###

        cd avalanche/
		make init
		make alldata

## Running ##
Once that's completed you can boot up the dev server:

        make run

Open browser to [http://127.0.0.1:8000](http://127.0.0.1:8000) -- if all went well you should see the home page.

### Other Tools ###
There are some other builtin commands for the makefile:

                make init       - installs all requirements / syncs db / migrates
                make docs       - makes docs with sphinx
                make test       - runs nosetests with coverage
                make data       - syncs db and migrations 
                make alldata    - loads all sample data fixtures
                make static     - runs python manage.py collectstatic to copy all static files to universal folder
        	make clean      - clears static files and db
        	make reset      - deletes everything and reloads database / fixtures
        	make run        - run's development server at localhost:8000


