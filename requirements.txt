--extra-index-url=http://dist.pinaxproject.com/dev/
--extra-index-url=http://dist.pinaxproject.com/alpha/

Django
metron==1.0
django-admin-tools
django-debug-toolbar
django-extensions
wadofstuff-django-serializers
feedparser
south
sphinx
nose #test runner
coverage #test coverage
pep8
pyflakes
pylint
pillow
mimeparse
python-dateutil
django-tastypie
requests

#load external bitbucket repo apps
git+https://github.com/jorgebastida/django-dajax.git
git+git://github.com/pinax/django-user-accounts.git
git+git://github.com/pinax/pinax-theme-bootstrap.git
git+git://github.com/pinax/pinax-theme-bootstrap-account.git
git+git://github.com/pinax/django-forms-bootstrap.git
git+https://github.com/maraujop/django-crispy-forms.git
git+https://github.com/pinax/pinax-utils.git
git+git://github.com/jbalogh/django-nose.git#egg=django-nose
hg+https://bitbucket.org/schinckel/django-jsonfield#egg=jsonfield
hg+https://bitbucket.org/neithere/django-autoslug#
git+https://github.com/jmcclell/django-bootstrap-pagination.git
git+https://github.com/riccardo-forina/django-admin-bootstrapped.git
git+git://github.com/davedash/django-fixture-magic.git
