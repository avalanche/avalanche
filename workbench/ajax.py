from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from django.utils import simplejson
from workbench.parse import parameterParser, apiGet
import requests


def serialize(modelid):
    try:
        host = 'http://127.0.0.1:8000/api/v1/workflow/%s/?format=json' % modelid
        r = requests.get(host)
        response = simplejson.dumps(r.json)
    except Exception, e:
        response = 'error occured in json %s' % e

    #print(response)
    return response


def keyExists(key, dict):
    try:
        return dict[key]
    except KeyError, e:
        return e


@dajaxice_register
def updateadmincombo(request, option):
    dajax = Dajax()

    data = apiGet('block_list')
    params = parameterParser(data, option, 'list')

    field = []

    if params:
        #for c in classes:
        field.append('<fieldset class="module _aligned" id="fparam"  style="background:transparent">')
        field.append('<h4>%s</h4><br>' % option)
        field.append('<div class="fields">')

        for p in params:
            vals = p[1]
            field.append('<div class="control-group field-name">')
            field.append('<div>')
            field.append(
                '<div class="control-label"><label for="id_%(lbl)s" class="required">%(lbl)s:</label></div>' % {
                'lbl': p[0]})

            #using default values and input types
            field.append(
                '<div class="controls"><input type="%(type)s" id="id_%(id)s" name="%(id)s" value="%(default)s">' % {
                    'type': 'input', 'id': p[0],
                    'default': keyExists(
                        'default',
                        vals)})

            field.append('<p class="help-block">%s</p>' % vals['desc'])
            field.append('</div></div></div>')

        field.append('</fieldset>')
        dajax.append("#blockparams", 'innerHTML', ''.join(field))

    return dajax.json()


def post(payload):
    r = requests.post(url="http://127.0.0.1:5000/workflow/in/true", data=payload)
    print r.status_code
    #now we wait for result from backend


@dajaxice_register
def run(request, text):
    json = serialize(text)
    post(json)
    return simplejson.dumps({'message': json})
