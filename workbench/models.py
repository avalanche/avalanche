import jsonfield
from django.db import models
from django_extensions.db.models import TimeStampedModel
from django_extensions.db.fields import UUIDField
from autoslug import AutoSlugField

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class WorkFlow(TimeStampedModel):
    name = models.CharField(max_length=200)
    blockmap = jsonfield.JSONField()
    created_by = models.ForeignKey(User, related_name='workflows')
    uuid = UUIDField(version=4, unique=True)
    slug = AutoSlugField(populate_from='name',
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ["-name"]

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        super(WorkFlow, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('workflow_edit', kwargs={'pk': self.pk})



class Block(TimeStampedModel):
    name = models.CharField(max_length=200)
    uuid = UUIDField(version=4, unique=True)
    params = models.CharField(max_length=200, blank=True)
    paramdata = jsonfield.JSONField(blank=True)
    workflow = models.ForeignKey(WorkFlow, blank=True, null=True)
    slug = AutoSlugField(populate_from='name',
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        super(Block, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('block_edit', kwargs={'pk': self.pk})



class Route(TimeStampedModel):
    name = models.CharField(max_length=200)
    uuid = UUIDField(version=4, unique=True)
    workflow = models.ForeignKey(WorkFlow, blank=True, null=True)
    inputslot = models.IntegerField(blank=True)
    outputslot = models.IntegerField(blank=True)
    inputs = models.ForeignKey(
        Block, to_field="uuid", related_name='source_set')
    outputs = models.ForeignKey(
        Block, to_field="uuid", related_name='target_set')
    slug = AutoSlugField(populate_from='name',
                         slugify=lambda value: value.replace(' ', '-'))

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        super(Route, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('route_edit', kwargs={'pk': self.pk})
