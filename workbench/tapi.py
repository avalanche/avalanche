#!/usr/bin/env python
import parse
import sys
import requests
from requests.auth import HTTPBasicAuth
from parse import AutoVivification
import json


def postresult():
    host = "http://127.0.0.1:8000/results/api/v1/result/?format=json"
    payload = {"data": "{}", "id": "2", "uuid": "a473271a-fc21-4527-a8e2-55094b6e04e4"}
    r = requests.post(url=host, auth=HTTPBasicAuth('admin', 'admin'), data=json.dumps(payload))
    #r = requests.get(url=host, auth=HTTPBasicAuth('admin', 'admin'))
    print(r.status_code)


def testapiget():
    response = parse.apiGet("block_list")

    #print("Blocks:")
    #print(parse.blockParser(response, 'tuple'))
    #print("\n")
    print("Params for RandomNumberSource:")
    params = parse.parameterParser(response, "RandomNumberSource", 'list')
    print(params)
    print(type(params))

    for p in params:
        vals = p[1]
        print(vals['default'])
        #vals = list(params[1])
        #print(params[0][1])
        #print(vals[1])


def parseresult():
    response = parse.apiParse("workflow")
    #print(response)
    #data = AutoVivification(response)
    data = response
    #print(response)
    pconnections = {}
    pblocks = {}
    #print(data['connections'])
    newdata = {'wb': { 'workflows': { }}}
    c1 = 1
    c2 = 1

    for blocks in data['blocks']:
        blockid= blocks['uuid']

        if blockid not in blocks:
            blocks.update({blockid:None})
            pblocks[blockid] = {'class': blocks['params'], 'params': blocks['paramdata']}
         
        #print(blocks)   
    print(json.dumps(blocks))   

def blocktest():
    model = sys.argv[1]
    host = "http://127.0.0.1:8000/results/api/v1/result/"
    r = requests.get('%s%s?format=json' % (host, model))
    print(json.dumps(r.json))


def backendtest(test):
    host = "http://127.0.0.1:5000/workflow/in/true"
    payload = json.dumps({"workflows": {
        "blocks": {
            "b0": {
                "class": "MessageProxy",
                "params": {
                    "message": "message",
                }
            },
            "b1": {
                "class": "MessageProxy",
                "params": {}
            }
        },
        "connections": {
            "b0": {
                "in": {
                    "0": None
                },
                "out": {
                    "0": "b1"
                }
            },
            "b1": {
                "in": {
                    "0": "b0"
                },
                "out": {
                    "0": None
                }
            }
        }
    }
    })

    r = requests.post(url=host, data=payload)
    print(r.status_code)
    #print(json.dumps(r.json))


def main():
    #backendtest(test="test")
    #postresult()
    parseresult()

if __name__ == '__main__':
    main()
