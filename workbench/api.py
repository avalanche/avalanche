from tastypie.resources import ModelResource
from tastypie.serializers import Serializer
from tastypie import fields
from workbench.models import WorkFlow, Block, Route

class WorkFlowResource2(ModelResource):
 class Meta:
        queryset = WorkFlow.objects.all()
        resource_name = 'workflow'
        excludes = ['blockmap', 'created', 'modified', 'slug', 'name', 'uuid']
        include_resource_uri = False
        serializer = Serializer()



class BlockResource(ModelResource):
    workflow = fields.ForeignKey(WorkFlowResource2, 'workflow', full=True)

    class Meta:
        queryset = Block.objects.all()
        resource_name = 'block'
        excludes = ['created', 'modified', 'slug']
        serializer = Serializer()
        include_resource_uri = False
        allowed_methods = ['get', 'post']


class RouteResource(ModelResource):
    inputs = fields.ForeignKey(BlockResource, 'inputs', full=True)
    outputs = fields.ForeignKey(BlockResource, 'outputs', full=True)

    class Meta:
        queryset = Route.objects.all()
        resource_name = 'route'
        excludes = ['created', 'modified', 'slug']
        include_resource_uri = False
        serializer = Serializer()
        allowed_methods = ['get', 'post']


class WorkFlowResource(ModelResource):
    connections = fields.ToManyField(RouteResource, attribute=lambda bundle: Route.objects.filter(workflow=bundle.obj),
                                     full=True)
    # blocks = fields.ToManyField(BlockResource, attribute=lambda bundle: Block.objects.filter(workflow=bundle.obj),
    #                                  full=True) 

    class Meta:
        queryset = WorkFlow.objects.all()
        resource_name = 'workflow'
        excludes = ['blockmap', 'created', 'modified', 'slug']
        include_resource_uri = False
        serializer = Serializer()
        #allowed_methods = ['get']
