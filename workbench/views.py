import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import ListView, CreateView, DeleteView, TemplateView
from django.core.urlresolvers import reverse_lazy
from workbench.models import WorkFlow, Block
from workbench.forms import WorkFlowForm, BlockForm


def workflow_save():
    pass


def block_create(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = BlockForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            #print("saved block!")
            return HttpResponseRedirect('/thanks/')  # Redirect after POST
    else:
        form = BlockForm()  # An unbound form

    return render(request, 'workflow/block_create.html', {
        'form': form,
    })


# def block_view(request, block_id=None):
#     #profile = request.user.get_profile()
#     if block_id is None:
#         block = WorkFlow(workflow=workflow)
#         template_title = _(u'Add Block')
#     else:
#         block = get_object_or_404(workbench.workflow.block_set.all(), pk=block_id)
#         template_title = _(u'Edit Block')
#     if request.POST:
#         if request.POST.get('cancel', None):
#             return HttpResponseRedirect('/')
#         form = BlockForm(block_id, request.POST, instance=block)
#         if form.is_valid():
#             block = form.save()
#             return HttpResponseRedirect('/')
#     else:
#         form = BlockForm(instance=block, workflow=workbench.workflow)
#     variables = RequestContext(request, {'form': form, 'template_title': template_title})
#     return render_to_response("block.html", variables)


class BlockEdit(TemplateView):
    model = Block
    template_name = "workflow/block_edit.html"

    def get_context_data(self, **kwargs):
        context = super(BlockEdit, self).get_context_data(**kwargs)
        context['block'] = Block.objects.get(
            pk=self.kwargs.get('pk', None))
        return context


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return super(AjaxableResponseMixin, self).form_invalid(form)

    def form_valid(self, form):
        if self.request.is_ajax():
            data = {
                'pk': form.instance.pk,
            }
            return self.render_to_json_response(data)
        else:
            return super(AjaxableResponseMixin, self).form_valid(form)


class WorkFlowCreate(AjaxableResponseMixin, CreateView):
    form_class = WorkFlowForm
    template_name = "workflow/workflow_create.html"
    model = WorkFlow

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(WorkFlowCreate, self).form_valid(form)


class WorkFlowEdit(TemplateView):
    model = WorkFlow
    template_name = "workflow/workflow_edit.html"

    def get_context_data(self, **kwargs):
        context = super(WorkFlowEdit, self).get_context_data(**kwargs)
        context['workflow'] = WorkFlow.objects.get(
            pk=self.kwargs.get('pk', None))
        return context


class WorkFlowDelete(DeleteView):
    model = WorkFlow
    success_url = reverse_lazy('workflow_list')


class WorkFlowList(ListView):
    template_name = "workflow/workflow_list.html"
    model = WorkFlow

    def get_queryset(self):
        if self.request.user.is_authenticated():
            return WorkFlow.objects.filter(created_by=self.request.user.pk)
