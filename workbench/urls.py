from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from tastypie.api import Api
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from workbench.api import WorkFlowResource, BlockResource, RouteResource
from workbench.views import WorkFlowCreate, WorkFlowList, WorkFlowDelete, WorkFlowEdit, BlockEdit

admin.autodiscover()
#setup ajax calls
dajaxice_autodiscover()

#setup api resources
v1_api = Api(api_name='v1')
v1_api.register(WorkFlowResource())
v1_api.register(BlockResource())
v1_api.register(RouteResource())

urlpatterns = patterns('',
                       #default urls here
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^admin_tools/', include('admin_tools.urls')),
                       #url(r'^docs/$', 'docs'),
                       url(r'^about/', TemplateView.as_view(template_name="about.html")),
                       url(r'^account/', include("account.urls")),
                       url(dajaxice_config.dajaxice_url,
                           include('dajaxice.urls')),
                       url(r'^api/', include(v1_api.urls)),
                       #workbench urls
                       url(r'block/(?P<pk>\d+)/$', BlockEdit.as_view(),
                           name='block_edit'),
                       url(r'^$', WorkFlowList.as_view(), name="home"),
                       url(r'^workflow/add/',
                           WorkFlowCreate.as_view(), name="workflow_add"),
                       url(r'^workflow/save/', 'workbench.views.workflow_save',
                           name="workflow_save"),
                       url(
                       r'workflow/(?P<pk>\d+)/delete/$', WorkFlowDelete.as_view(),
                       name='workflow_delete'),
                       url(r'workflow/(?P<pk>\d+)/$', WorkFlowEdit.as_view(),
                           name='workflow_edit'),
                       url(r'^block/create/', 'workbench.views.block_create',
                           name="block_create"),
                       url(r'^results/', include('results.urls')),
                       url(r'^data/', include('datasets.urls')),
                       #restaurant frontend urls
                       url(r'^wines/', include('wines.urls')),
                       url(r'^beers/', include('beers.urls')),
                       url(r'^home/', include('home.urls')),
                       url(r'^cart/', include('cart.urls')),
                       url(r'^orders/', include('orders.urls')),
                       )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
