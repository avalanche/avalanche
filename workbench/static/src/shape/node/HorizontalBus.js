/**
Library is under GPL License (GPL)

Copyright (c) 2012 Andreas Herz

**/



/**
 * @class draw2d.shape.node.HorizontalBus
 * 
 * A horizontal bus shape with a special kind of port handling. The hole figure is a hybrid port.
 * 
 * See the example:
 *
 *     @example preview small frame
 *     
 *     var figure =  new draw2d.shape.node.HorizontalBus(300,20,"Horizontal Bus");
 *     
 *     canvas.addFigure(figure,50,10);
 *     
 * @extends draw2d.shape.node.Hub
 */
draw2d.shape.node.HorizontalBus = draw2d.shape.node.Hub.extend({

    NAME : "draw2d.shape.node.HorizontalBus",

	/**
	 * 
	 * @param {Number} width initial width of the bus shape
	 * @param {Number} height height of the bus
	 */
	init : function(width, height, label)
    {
        this._super(width,height,label);
    },
    
    /**
     * @method
     * Callback to update the visibility of the resize handles
     * 
     * @param {draw2d.Canvas} canvas
     * @param {draw2d.ResizeHandle} resizeHandle1 topLeft resize handle
     * @param {draw2d.ResizeHandle} resizeHandle2 topCenter resize handle
     * @param {draw2d.ResizeHandle} resizeHandle3 topRight resize handle
     * @param {draw2d.ResizeHandle} resizeHandle4 rightMiddle resize handle
     * @param {draw2d.ResizeHandle} resizeHandle5 bottomRight resize handle
     * @param {draw2d.ResizeHandle} resizeHandle6 bottomCenter resize handle
     * @param {draw2d.ResizeHandle} resizeHandle7 bottomLeft resize handle
     * @param {draw2d.ResizeHandle} resizeHandle8 leftMiddle resize handle
     * @template
     */
     showResizeHandles: function(canvas, resizeHandle1, resizeHandle2, resizeHandle3, resizeHandle4, resizeHandle5, resizeHandle6, resizeHandle7, resizeHandle8)
     {
      	resizeHandle4.setDimension(resizeHandle4.getWidth(), this.getHeight());
     	resizeHandle8.setDimension(resizeHandle4.getWidth(), this.getHeight());
     	 
     	this._super(canvas, resizeHandle1, resizeHandle2, resizeHandle3, resizeHandle4, resizeHandle5, resizeHandle6, resizeHandle7, resizeHandle8);
   	
        resizeHandle1.hide();
       // resizeHandle2.hide();
        resizeHandle3.hide();
        resizeHandle5.hide();
      //  resizeHandle6.hide();
        resizeHandle7.hide();
     },
     
     /**
      * @method
      * Callback to update the visibility of the resize handles
      * 
      * @param {draw2d.Canvas} canvas
      * @param {draw2d.ResizeHandle} resizeHandle1 topLeft resize handle
      * @param {draw2d.ResizeHandle} resizeHandle2 topCenter resize handle
      * @param {draw2d.ResizeHandle} resizeHandle3 topRight resize handle
      * @param {draw2d.ResizeHandle} resizeHandle4 rightMiddle resize handle
      * @param {draw2d.ResizeHandle} resizeHandle5 bottomRight resize handle
      * @param {draw2d.ResizeHandle} resizeHandle6 bottomCenter resize handle
      * @param {draw2d.ResizeHandle} resizeHandle7 bottomLeft resize handle
      * @param {draw2d.ResizeHandle} resizeHandle8 leftMiddle resize handle
      * @template
      */
     moveResizeHandles: function(canvas, resizeHandle1, resizeHandle2, resizeHandle3, resizeHandle4, resizeHandle5, resizeHandle6, resizeHandle7, resizeHandle8)
     {
         // adjust the resize handles on the left/right to the new dimension of the shape
         //
         resizeHandle4.setDimension(resizeHandle4.getWidth(), this.getHeight());
         resizeHandle8.setDimension(resizeHandle4.getWidth(), this.getHeight());
          
         this._super(canvas, resizeHandle1, resizeHandle2, resizeHandle3, resizeHandle4, resizeHandle5, resizeHandle6, resizeHandle7, resizeHandle8);
     }
     

});
