/**
Library is under GPL License (GPL)

Copyright (c) 2012 Andreas Herz

**/


/**
 * @class draw2d
 * global namespace declarations
 * 
 * @private
 */
var draw2d = 
{
    geo: {
    },

    io:{
        json:{},
        png:{},
        svg:{}  
    },
    
    util : {
    },

    shape : {
    	basic:{},
        arrow:{},
        node: {},
        note: {},
        diagram:{},
        analog:{},
        icon:{},
        widget:{}
    },
    
    policy : {
    },
    
    command : {
    },

    decoration:{
    	connection:{}
    }, 
    
    layout: {
        connection :{},
	    anchor :{},
	    mesh :{},
	    locator: {}
    },
    
    
    ui :{
    	
    },
    
    isTouchDevice : (
            //Detect iPhone
            (navigator.platform.indexOf("iPhone") != -1) ||
            //Detect iPod
            (navigator.platform.indexOf("iPod") != -1)||
            //Detect iPad
            (navigator.platform.indexOf("iPad") != -1)
        )
    
};

if(typeof draw2dPath === "undefined"){
    draw2dPath = "../../";
}

// loading the lib
//
$LAB
.script(draw2dPath+"lib/shifty.js")
.script(draw2dPath+"lib/raphael.js")
.script(draw2dPath+"lib/jquery-1.8.1.min.js").wait()
.script(draw2dPath+"lib/jquery-ui-1.8.23.custom.min.js").wait()
.script(draw2dPath+"lib/jquery.layout.js")
.script(draw2dPath+"lib/jquery.autoresize.js")
.script(draw2dPath+"lib/jquery-touch_punch.js")
.script(draw2dPath+"lib/jquery.contextmenu.js")
.script(draw2dPath+"lib/rgbcolor.js")
.script(draw2dPath+"lib/canvg.js")
.script(draw2dPath+"lib/Class.js")
.script(draw2dPath+"lib/json2.js").wait()

.script(draw2dPath+"src/util/Color.js")
.script(draw2dPath+"src/util/ArrayList.js")
.script(draw2dPath+"src/util/SVGUtil.js")
.script(draw2dPath+"src/util/UUID.js")
.script(draw2dPath+"src/geo/PositionConstants.js")
.script(draw2dPath+"src/geo/Point.js")
.script(draw2dPath+"src/geo/Rectangle.js")

.script(draw2dPath+"src/command/CommandType.js")
.script(draw2dPath+"src/command/Command.js")
.script(draw2dPath+"src/command/CommandStack.js")
.script(draw2dPath+"src/command/CommandStackEvent.js")
.script(draw2dPath+"src/command/CommandStackEventListener.js")
.script(draw2dPath+"src/command/CommandMove.js")
.script(draw2dPath+"src/command/CommandResize.js")
.script(draw2dPath+"src/command/CommandConnect.js")
.script(draw2dPath+"src/command/CommandReconnect.js")
.script(draw2dPath+"src/command/CommandDelete.js")
.script(draw2dPath+"src/command/CommandAdd.js")

.script(draw2dPath+"src/layout/connection/ConnectionRouter.js")
.script(draw2dPath+"src/layout/connection/DirectRouter.js")
.script(draw2dPath+"src/layout/connection/ManhattanConnectionRouter.js")
.script(draw2dPath+"src/layout/connection/ManhattanBridgedConnectionRouter.js")
.script(draw2dPath+"src/layout/connection/BezierConnectionRouter.js")
.script(draw2dPath+"src/layout/connection/FanConnectionRouter.js")

.script(draw2dPath+"src/layout/mesh/MeshLayouter.js")
.script(draw2dPath+"src/layout/mesh/ExplodeLayouter.js")
.script(draw2dPath+"src/layout/mesh/ProposedMeshChange.js")

.script(draw2dPath+"src/layout/locator/Locator.js")
.script(draw2dPath+"src/layout/locator/PortLocator.js")
.script(draw2dPath+"src/layout/locator/InputPortLocator.js")
.script(draw2dPath+"src/layout/locator/OutputPortLocator.js")
.script(draw2dPath+"src/layout/locator/ConnectionLocator.js")
.script(draw2dPath+"src/layout/locator/ManhattanMidpointLocator.js")
.script(draw2dPath+"src/layout/locator/TopLocator.js")
.script(draw2dPath+"src/layout/locator/BottomLocator.js")
.script(draw2dPath+"src/layout/locator/LeftLocator.js")
.script(draw2dPath+"src/layout/locator/RightLocator.js")
.script(draw2dPath+"src/layout/locator/CenterLocator.js")

.script(draw2dPath+"src/policy/EditPolicy.js")
.script(draw2dPath+"src/policy/DragDropEditPolicy.js")
.script(draw2dPath+"src/policy/RegionEditPolicy.js")
.script(draw2dPath+"src/policy/HorizontalEditPolicy.js")
.script(draw2dPath+"src/policy/VerticalEditPolicy.js")

.script(draw2dPath+"src/Canvas.js")
.script(draw2dPath+"src/Figure.js")
.script(draw2dPath+"src/shape/node/Node.js")
.script(draw2dPath+"src/VectorFigure.js")
.script(draw2dPath+"src/shape/basic/Rectangle.js")
.script(draw2dPath+"src/SetFigure.js")
.script(draw2dPath+"src/SVGFigure.js")
.script(draw2dPath+"src/shape/node/Hub.js").wait()
.script(draw2dPath+"src/shape/node/HorizontalBus.js")
.script(draw2dPath+"src/shape/node/VerticalBus.js")

.script(draw2dPath+"src/shape/basic/Oval.js")
.script(draw2dPath+"src/shape/basic/Circle.js")
.script(draw2dPath+"src/shape/basic/Label.js")
.script(draw2dPath+"src/shape/basic/Line.js")
.script(draw2dPath+"src/shape/basic/PolyLine.js")
.script(draw2dPath+"src/shape/basic/Diamond.js")
.script(draw2dPath+"src/shape/basic/Image.js")
.script(draw2dPath+"src/Connection.js")
.script(draw2dPath+"src/VectorFigure.js")
.script(draw2dPath+"src/ResizeHandle.js")
.script(draw2dPath+"src/LineResizeHandle.js")
.script(draw2dPath+"src/LineStartResizeHandle.js")
.script(draw2dPath+"src/LineEndResizeHandle.js")
.script(draw2dPath+"src/Port.js").wait()
.script(draw2dPath+"src/InputPort.js")
.script(draw2dPath+"src/OutputPort.js")
.script(draw2dPath+"src/HybridPort.js")

.script(draw2dPath+"src/layout/anchor/ConnectionAnchor.js")
.script(draw2dPath+"src/layout/anchor/ChopboxConnectionAnchor.js")
.script(draw2dPath+"src/layout/anchor/ShortesPathConnectionAnchor.js")

.script(draw2dPath+"src/shape/arrow/CalligrapherArrowLeft.js")
.script(draw2dPath+"src/shape/arrow/CalligrapherArrowDownLeft.js")

.script(draw2dPath+"src/shape/node/Start.js")
.script(draw2dPath+"src/shape/node/End.js")
.script(draw2dPath+"src/shape/node/Between.js")

.script(draw2dPath+"src/shape/note/PostIt.js")

.script(draw2dPath+"src/shape/widget/Widget.js")
.script(draw2dPath+"src/shape/widget/Slider.js")

.script(draw2dPath+"src/shape/diagram/Diagram.js")
.script(draw2dPath+"src/shape/diagram/Pie.js")
.script(draw2dPath+"src/shape/diagram/Sparkline.js")

.script(draw2dPath+"src/shape/analog/OpAmp.js")
.script(draw2dPath+"src/shape/analog/ResistorBridge.js")
.script(draw2dPath+"src/shape/analog/ResistorVertical.js")
.script(draw2dPath+"src/shape/analog/VoltageSupplyHorizontal.js")
.script(draw2dPath+"src/shape/analog/VoltageSupplyVertical.js")

.script(draw2dPath+"src/shape/icon/Icon.js")
.script(draw2dPath+"src/shape/icon/Thunder.js")
.script(draw2dPath+"src/shape/icon/Snow.js")
.script(draw2dPath+"src/shape/icon/Hail.js")
.script(draw2dPath+"src/shape/icon/Rain.js")
.script(draw2dPath+"src/shape/icon/Cloudy.js")
.script(draw2dPath+"src/shape/icon/Sun.js")
.script(draw2dPath+"src/shape/icon/Undo.js")
.script(draw2dPath+"src/shape/icon/Detour.js")
.script(draw2dPath+"src/shape/icon/Merge.js")
.script(draw2dPath+"src/shape/icon/Split.js")
.script(draw2dPath+"src/shape/icon/Fork.js")
.script(draw2dPath+"src/shape/icon/ForkAlt.js")
.script(draw2dPath+"src/shape/icon/Exchange.js")
.script(draw2dPath+"src/shape/icon/Shuffle.js")
.script(draw2dPath+"src/shape/icon/Refresh.js")
.script(draw2dPath+"src/shape/icon/Ccw.js")
.script(draw2dPath+"src/shape/icon/Acw.js")
.script(draw2dPath+"src/shape/icon/Contract.js")
.script(draw2dPath+"src/shape/icon/Expand.js")
.script(draw2dPath+"src/shape/icon/Stop.js")
.script(draw2dPath+"src/shape/icon/End.js")
.script(draw2dPath+"src/shape/icon/Start.js")
.script(draw2dPath+"src/shape/icon/Ff.js")
.script(draw2dPath+"src/shape/icon/Rw.js")
.script(draw2dPath+"src/shape/icon/ArrowRight.js")
.script(draw2dPath+"src/shape/icon/ArrowLeft.js")
.script(draw2dPath+"src/shape/icon/ArrowUp.js")
.script(draw2dPath+"src/shape/icon/ArrowDown.js")
.script(draw2dPath+"src/shape/icon/ArrowLeft2.js")
.script(draw2dPath+"src/shape/icon/ArrowRight2.js")
.script(draw2dPath+"src/shape/icon/Smile2.js")
.script(draw2dPath+"src/shape/icon/Smile.js")
.script(draw2dPath+"src/shape/icon/Alarm.js")
.script(draw2dPath+"src/shape/icon/Clock.js")
.script(draw2dPath+"src/shape/icon/StopWatch.js")
.script(draw2dPath+"src/shape/icon/History.js")
.script(draw2dPath+"src/shape/icon/Future.js")
.script(draw2dPath+"src/shape/icon/GlobeAlt2.js")
.script(draw2dPath+"src/shape/icon/GlobeAlt.js")
.script(draw2dPath+"src/shape/icon/Globe.js")
.script(draw2dPath+"src/shape/icon/Warning.js")
.script(draw2dPath+"src/shape/icon/Code.js")
.script(draw2dPath+"src/shape/icon/Pensil.js")
.script(draw2dPath+"src/shape/icon/Pen.js")
.script(draw2dPath+"src/shape/icon/Plus.js")
.script(draw2dPath+"src/shape/icon/Minus.js")
.script(draw2dPath+"src/shape/icon/TShirt.js")
.script(draw2dPath+"src/shape/icon/Sticker.js")
.script(draw2dPath+"src/shape/icon/Page2.js")
.script(draw2dPath+"src/shape/icon/Page.js")
.script(draw2dPath+"src/shape/icon/Landscape1.js")
.script(draw2dPath+"src/shape/icon/Landscape2.js")
.script(draw2dPath+"src/shape/icon/Plugin.js")
.script(draw2dPath+"src/shape/icon/Bookmark.js")
.script(draw2dPath+"src/shape/icon/Hammer.js")
.script(draw2dPath+"src/shape/icon/Users.js")
.script(draw2dPath+"src/shape/icon/User.js")
.script(draw2dPath+"src/shape/icon/Customer.js")
.script(draw2dPath+"src/shape/icon/Employee.js")
.script(draw2dPath+"src/shape/icon/Anonymous.js")
.script(draw2dPath+"src/shape/icon/Skull.js")
.script(draw2dPath+"src/shape/icon/Mail.js")
.script(draw2dPath+"src/shape/icon/Picture.js")
.script(draw2dPath+"src/shape/icon/Bubble.js")
.script(draw2dPath+"src/shape/icon/CodeTalk.js")
.script(draw2dPath+"src/shape/icon/Talkq.js")
.script(draw2dPath+"src/shape/icon/Talke.js")
.script(draw2dPath+"src/shape/icon/Home.js")
.script(draw2dPath+"src/shape/icon/Lock.js")
.script(draw2dPath+"src/shape/icon/Clip.js")
.script(draw2dPath+"src/shape/icon/Star.js")
.script(draw2dPath+"src/shape/icon/StarOff.js")
.script(draw2dPath+"src/shape/icon/Star2.js")
.script(draw2dPath+"src/shape/icon/Star2Off.js")
.script(draw2dPath+"src/shape/icon/Star3.js")
.script(draw2dPath+"src/shape/icon/Star3Off.js")
.script(draw2dPath+"src/shape/icon/Chat.js")
.script(draw2dPath+"src/shape/icon/Quote.js")
.script(draw2dPath+"src/shape/icon/Gear2.js")
.script(draw2dPath+"src/shape/icon/Gear.js")
.script(draw2dPath+"src/shape/icon/Wrench.js")
.script(draw2dPath+"src/shape/icon/Wrench2.js")
.script(draw2dPath+"src/shape/icon/Wrench3.js")
.script(draw2dPath+"src/shape/icon/ScrewDriver.js")
.script(draw2dPath+"src/shape/icon/HammerAndScrewDriver.js")
.script(draw2dPath+"src/shape/icon/Magic.js")
.script(draw2dPath+"src/shape/icon/Download.js")
.script(draw2dPath+"src/shape/icon/View.js")
.script(draw2dPath+"src/shape/icon/Noview.js")
.script(draw2dPath+"src/shape/icon/Cloud.js")
.script(draw2dPath+"src/shape/icon/Cloud2.js")
.script(draw2dPath+"src/shape/icon/CloudDown.js")
.script(draw2dPath+"src/shape/icon/CloudUp.js")
.script(draw2dPath+"src/shape/icon/Location.js")
.script(draw2dPath+"src/shape/icon/Volume0.js")
.script(draw2dPath+"src/shape/icon/Volume1.js")
.script(draw2dPath+"src/shape/icon/Volume2.js")
.script(draw2dPath+"src/shape/icon/Volume3.js")
.script(draw2dPath+"src/shape/icon/Key.js")
.script(draw2dPath+"src/shape/icon/Ruler.js")
.script(draw2dPath+"src/shape/icon/Power.js")
.script(draw2dPath+"src/shape/icon/Unlock.js")
.script(draw2dPath+"src/shape/icon/Flag.js")
.script(draw2dPath+"src/shape/icon/Tag.js")
.script(draw2dPath+"src/shape/icon/Search.js")
.script(draw2dPath+"src/shape/icon/ZoomOut.js")
.script(draw2dPath+"src/shape/icon/ZoomIn.js")
.script(draw2dPath+"src/shape/icon/Cross.js")
.script(draw2dPath+"src/shape/icon/Check.js")
.script(draw2dPath+"src/shape/icon/Settings.js")
.script(draw2dPath+"src/shape/icon/SettingsAlt.js")
.script(draw2dPath+"src/shape/icon/Feed.js")
.script(draw2dPath+"src/shape/icon/Bug.js")
.script(draw2dPath+"src/shape/icon/Link.js")
.script(draw2dPath+"src/shape/icon/Calendar.js")
.script(draw2dPath+"src/shape/icon/Picker.js")
.script(draw2dPath+"src/shape/icon/No.js")
.script(draw2dPath+"src/shape/icon/CommandLine.js")
.script(draw2dPath+"src/shape/icon/Photo.js")
.script(draw2dPath+"src/shape/icon/Printer.js")
.script(draw2dPath+"src/shape/icon/Export.js")
.script(draw2dPath+"src/shape/icon/Import.js")
.script(draw2dPath+"src/shape/icon/Run.js")
.script(draw2dPath+"src/shape/icon/Magnet.js")
.script(draw2dPath+"src/shape/icon/NoMagnet.js")
.script(draw2dPath+"src/shape/icon/ReflectH.js")
.script(draw2dPath+"src/shape/icon/ReflectV.js")
.script(draw2dPath+"src/shape/icon/Resize2.js")
.script(draw2dPath+"src/shape/icon/Rotate.js")
.script(draw2dPath+"src/shape/icon/Connect.js")
.script(draw2dPath+"src/shape/icon/Disconnect.js")
.script(draw2dPath+"src/shape/icon/Folder.js")
.script(draw2dPath+"src/shape/icon/Man.js")
.script(draw2dPath+"src/shape/icon/Woman.js")
.script(draw2dPath+"src/shape/icon/People.js")
.script(draw2dPath+"src/shape/icon/Parent.js")
.script(draw2dPath+"src/shape/icon/Notebook.js")
.script(draw2dPath+"src/shape/icon/Diagram.js")
.script(draw2dPath+"src/shape/icon/BarChart.js")
.script(draw2dPath+"src/shape/icon/PieChart.js")
.script(draw2dPath+"src/shape/icon/LineChart.js")
.script(draw2dPath+"src/shape/icon/Apps.js")
.script(draw2dPath+"src/shape/icon/Locked.js")
.script(draw2dPath+"src/shape/icon/Ppt.js")
.script(draw2dPath+"src/shape/icon/Lab.js")
.script(draw2dPath+"src/shape/icon/Umbrella.js")
.script(draw2dPath+"src/shape/icon/Dry.js")
.script(draw2dPath+"src/shape/icon/Ipad.js")
.script(draw2dPath+"src/shape/icon/Iphone.js")
.script(draw2dPath+"src/shape/icon/Jigsaw.js")
.script(draw2dPath+"src/shape/icon/Lamp.js")
.script(draw2dPath+"src/shape/icon/Lamp_alt.js")
.script(draw2dPath+"src/shape/icon/Video.js")
.script(draw2dPath+"src/shape/icon/Palm.js")
.script(draw2dPath+"src/shape/icon/Fave.js")
.script(draw2dPath+"src/shape/icon/Help.js")
.script(draw2dPath+"src/shape/icon/Crop.js")
.script(draw2dPath+"src/shape/icon/BioHazard.js")
.script(draw2dPath+"src/shape/icon/WheelChair.js")
.script(draw2dPath+"src/shape/icon/Mic.js")
.script(draw2dPath+"src/shape/icon/MicMute.js")
.script(draw2dPath+"src/shape/icon/IMac.js")
.script(draw2dPath+"src/shape/icon/Pc.js")
.script(draw2dPath+"src/shape/icon/Cube.js")
.script(draw2dPath+"src/shape/icon/FullCube.js")
.script(draw2dPath+"src/shape/icon/Font.js")
.script(draw2dPath+"src/shape/icon/Trash.js")
.script(draw2dPath+"src/shape/icon/NewWindow.js")
.script(draw2dPath+"src/shape/icon/DockRight.js")
.script(draw2dPath+"src/shape/icon/DockLeft.js")
.script(draw2dPath+"src/shape/icon/DockBottom.js")
.script(draw2dPath+"src/shape/icon/DockTop.js")
.script(draw2dPath+"src/shape/icon/Pallete.js")
.script(draw2dPath+"src/shape/icon/Cart.js")
.script(draw2dPath+"src/shape/icon/Glasses.js")
.script(draw2dPath+"src/shape/icon/Package.js")
.script(draw2dPath+"src/shape/icon/Book.js")
.script(draw2dPath+"src/shape/icon/Books.js")
.script(draw2dPath+"src/shape/icon/Icons.js")
.script(draw2dPath+"src/shape/icon/List.js")
.script(draw2dPath+"src/shape/icon/Db.js")
.script(draw2dPath+"src/shape/icon/Paper.js")
.script(draw2dPath+"src/shape/icon/TakeOff.js")
.script(draw2dPath+"src/shape/icon/Landing.js")
.script(draw2dPath+"src/shape/icon/Plane.js")
.script(draw2dPath+"src/shape/icon/Phone.js")
.script(draw2dPath+"src/shape/icon/HangUp.js")
.script(draw2dPath+"src/shape/icon/SlideShare.js")
.script(draw2dPath+"src/shape/icon/Twitter.js")
.script(draw2dPath+"src/shape/icon/TwitterBird.js")
.script(draw2dPath+"src/shape/icon/Skype.js")
.script(draw2dPath+"src/shape/icon/Windows.js")
.script(draw2dPath+"src/shape/icon/Apple.js")
.script(draw2dPath+"src/shape/icon/Linux.js")
.script(draw2dPath+"src/shape/icon/NodeJs.js")
.script(draw2dPath+"src/shape/icon/JQuery.js")
.script(draw2dPath+"src/shape/icon/Sencha.js")
.script(draw2dPath+"src/shape/icon/Vim.js")
.script(draw2dPath+"src/shape/icon/InkScape.js")
.script(draw2dPath+"src/shape/icon/Aumade.js")
.script(draw2dPath+"src/shape/icon/Firefox.js")
.script(draw2dPath+"src/shape/icon/Ie.js")
.script(draw2dPath+"src/shape/icon/Ie9.js")
.script(draw2dPath+"src/shape/icon/Opera.js")
.script(draw2dPath+"src/shape/icon/Chrome.js")
.script(draw2dPath+"src/shape/icon/Safari.js")
.script(draw2dPath+"src/shape/icon/LinkedIn.js")
.script(draw2dPath+"src/shape/icon/Flickr.js")
.script(draw2dPath+"src/shape/icon/GitHub.js")
.script(draw2dPath+"src/shape/icon/GitHubAlt.js")
.script(draw2dPath+"src/shape/icon/Raphael.js")
.script(draw2dPath+"src/shape/icon/GRaphael.js")
.script(draw2dPath+"src/shape/icon/Svg.js")
.script(draw2dPath+"src/shape/icon/Usb.js")
.script(draw2dPath+"src/shape/icon/Ethernet.js")
   
.script(draw2dPath+"src/ui/LabelEditor.js")
.script(draw2dPath+"src/ui/LabelInplaceEditor.js")

.script(draw2dPath+"src/decoration/connection/Decorator.js")
.script(draw2dPath+"src/decoration/connection/ArrowDecorator.js")
.script(draw2dPath+"src/decoration/connection/DiamondDecorator.js")
.script(draw2dPath+"src/decoration/connection/CircleDecorator.js")
.script(draw2dPath+"src/decoration/connection/BarDecorator.js")

.script(draw2dPath+"src/io/Reader.js")
.script(draw2dPath+"src/io/Writer.js")
.script(draw2dPath+"src/io/svg/Writer.js")
.script(draw2dPath+"src/io/png/Writer.js")
.script(draw2dPath+"src/io/json/Writer.js")
.script(draw2dPath+"src/io/json/Reader.js").wait(
function(){
	var link = $("<link>");
	link.attr({
	        type: 'text/css',
	        rel: 'stylesheet',
	        href: draw2dPath+"css/contextmenu.css"
	});
	$("head").append( link ); 

	// avoid iPad bounce effect during DragDrop
	//
    document.ontouchmove = function(e){e.preventDefault();};

    // hide context menu
    document.oncontextmenu = function() {return false;};
    
    
    // hacking RaphaelJS to support groups of elements
	//
	(function() {
	    Raphael.fn.group = function(f, g) {
	        var enabled = document.getElementsByTagName("svg").length > 0;
	        if (!enabled) {
	            // return a stub for VML compatibility
	            return {
	                add : function() {
	                    // intentionally left blank
	                }
	            };
	        }
	      var i;
	      this.svg = "http://www.w3.org/2000/svg";
	      this.defs = document.getElementsByTagName("defs")[f];
	      this.svgcanv = document.getElementsByTagName("svg")[f];
	      this.group = document.createElementNS(this.svg, "g");
	      for(i = 0;i < g.length;i++) {
	        this.group.appendChild(g[i].node);
	      }
	      this.svgcanv.appendChild(this.group);
	      this.group.translate = function(c, a) {
	        this.setAttribute("transform", "translate(" + c + "," + a + ") scale(" + this.getAttr("scale").x + "," + this.getAttr("scale").y + ")");
	      };
	      this.group.rotate = function(c, a, e) {
	        this.setAttribute("transform", "translate(" + this.getAttr("translate").x + "," + this.getAttr("translate").y + ") scale(" + this.getAttr("scale").x + "," + this.getAttr("scale").y + ") rotate(" + c + "," + a + "," + e + ")");
	      };
	      this.group.scale = function(c, a) {
	        this.setAttribute("transform", "scale(" + c + "," + a + ") translate(" + this.getAttr("translate").x + "," + this.getAttr("translate").y + ")");
	      };
	      this.group.push = function(c) {
	        this.appendChild(c.node);
	      };
	      this.group.getAttr = function(c) {
	        this.previous = this.getAttribute("transform") ? this.getAttribute("transform") : "";
	        var a = [], e, h, j;
	        a = this.previous.split(" ");
	        for(i = 0;i < a.length;i++) {
	          if(a[i].substring(0, 1) == "t") {
	            var d = a[i], b = [];
	            b = d.split("(");
	            d = b[1].substring(0, b[1].length - 1);
	            b = [];
	            b = d.split(",");
	            e = b.length === 0 ? {x:0, y:0} : {x:b[0], y:b[1]};
	          }else {
	            if(a[i].substring(0, 1) === "r") {
	              d = a[i];
	              b = d.split("(");
	              d = b[1].substring(0, b[1].length - 1);
	              b = d.split(",");
	              h = b.length === 0 ? {x:0, y:0, z:0} : {x:b[0], y:b[1], z:b[2]};
	            }else {
	              if(a[i].substring(0, 1) === "s") {
	                d = a[i];
	                b = d.split("(");
	                d = b[1].substring(0, b[1].length - 1);
	                b = d.split(",");
	                j = b.length === 0 ? {x:1, y:1} : {x:b[0], y:b[1]};
	              }
	            }
	          }
	        }
	        if(typeof e === "undefined") {
	          e = {x:0, y:0};
	        }
	        if(typeof h === "undefined") {
	          h = {x:0, y:0, z:0};
	        }
	        if(typeof j === "undefined") {
	          j = {x:1, y:1};
	        }
	        
	        if(c == "translate") {
	          var k = e;
	        }else {
	          if(c == "rotate") {
	            k = h;
	          }else {
	            if(c == "scale") {
	              k = j;
	            }
	          }
	        }
	        return k;
	      };
	      this.group.copy = function(el){
	         this.copy = el.node.cloneNode(true);
	         this.appendChild(this.copy);
	      };
	      return this.group;
	    };
	})();
	try{
	draw2dLoaded();
	}catch(exc){
	    console.log(exc);
	}
});

var _errorStack_=[];
function pushErrorStack(/*:Exception*/ e, /*:String*/ functionName)
{
  _errorStack_.push(functionName+"\n");
  /*re*/throw e;
}


Math.sign = function()
{
 if (this < 0) {return -1;}
 return 1;
};
