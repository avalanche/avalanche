/**
Library is under GPL License (GPL)

Copyright (c) 2012 Andreas Herz

**/



/**
 * @class draw2d.policy.DragDropEditPolicy
 * 
 * Called by the framework if the user edit the position of a figure with a drag drop operation.
 *
 * @author  Andreas Herz
 * @extends draw2d.policy.EditPolicy
 */
draw2d.policy.DragDropEditPolicy = draw2d.policy.EditPolicy.extend({

    /**
     * @constructor 
     * Creates a new Router object
     */
    init: function(){
        this._super();
    },
    
    /**
     * @method
     * Return the role of the edit policy
     * 
     * @template
     * @return the role of the policy.
     */
    getRole:function(){
        return draw2d.policy.EditPolicy.Role.PRIMARY_DRAG_ROLE;
    },
    
    /**
     * @method
     * Adjust the coordinates to the rectangle/region of this constraint.
     * 
     * @param figure
     * @param {Number|draw2d.geo.Point} x
     * @param {number} [y]
     * @returns {draw2d.geo.Point} the constraint position of the figure
     * 
     * @template
     */
    apply: function(figure, x,y){
        // do nothing per default implementation
        if(x instanceof draw2d.geo.Point){
            return x;
        }
        
        return new draw2d.geo.Point(x,y);
    }


});