// declare the namespace for this example
var workflow = {};

/**
 *
 * The **GraphicalEditor** is responsible for layout and dialog handling.
 *
 * @author Andreas Herz
 * @extends draw2d.ui.parts.GraphicalEditor
 */
workflow.Application = Class.extend(
    {
        NAME:"workflow.Application",

        /**
         * @constructor
         *
         * @param {String} canvasId the id of the DOM element to use as paint container
         */
        init:function (blmap) {

            // unmarshal the JSON document into the canvas
            this.reader = new draw2d.io.json.Reader();
            this.blmap = blmap;
            this.LabeledEnd = new workflow.LabeledEnd();
            this.LabeledStart = new workflow.LabeledStart();
            this.LabeledBetween = new workflow.LabeledBetween();
            this.view = new workflow.View("canvas");
            this.toolbar = new workflow.Toolbar("toolbar", this.view);



            this.reader.unmarshal(this.view, this.blmap);


            var data = document.getElementById('canvas');

        }

    });
