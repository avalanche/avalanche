workflow.LabeledBetween = draw2d.shape.node.Between.extend({

    init:function () {
        this._super();

         // Create any Draw2D figure as decoration for the connection
        //
        this.label = new draw2d.shape.basic.Label("Class");
        this.label.setColor("#0d0d0d");
        this.label.setFontColor("#0d0d0d");

        this.label2 = new draw2d.shape.basic.Label("Name");
        this.label2.setColor("#0d0d0d");
        this.label2.setFontColor("#0d0d0d");


        // add the new decoration to the connection with a position locator.
        //
        this.addFigure(this.label, new draw2d.layout.locator.BottomLocator(this));
        this.addFigure(this.label2, new draw2d.layout.locator.TopLocator(this));

        this.label2.installEditor(new draw2d.ui.LabelInplaceEditor());

    },

        /**
     * @method
     * Change the corner radius if the user clicks on the element. 
     * quite simple....
     * 
     */
    onDoubleClick: function(){
        alert('clicked');
        var url = "/block/1/"; // get the block form url
        $("#blockModal").load(url, function() { // load the url into the modal
            $(document).modal('show'); // display the modal on url load
        });
    },


    /**
     * @method
     * Return an objects with all important attributes for XML or JSON serialization
     *
     * @returns {Object}
     */
    getPersistentAttributes:function () {
        var memento = this._super();

        // add all decorations to the memento 
        //
        memento.labels = [];
        this.children.each(function (i, e) {
            memento.labels.push({
                id:e.figure.getId(),
                label:e.figure.getText(),
                locator:e.locator.NAME
            });
        });

        return memento;
    },

    /**
     * @method
     * Read all attributes from the serialized properties and transfer them into the shape.
     *
     * @param {Object} memento
     * @returns
     */
    setPersistentAttributes:function (memento) {
        this._super(memento);

        // remove all decorations created in the constructor of this element
        //
        this.resetChildren();

        // and restore all children of the JSON document instead.
        //
        $.each(memento.labels, $.proxy(function (i, e) {
            var label = new draw2d.shape.basic.Label(e.label);
            var locator = eval("new " + e.locator + "()");
            locator.setParent(this);
            this.addFigure(label, locator);
        }, this));
    },


    /**
     * @method
     * Called if the user drop this element onto the dropTarget. 
     * 
     * In this Example we create a "smart insert" of an existing connection.
     * COOL and fast network editing.
     * 
     * @param {draw2d.Figure} dropTarget The drop target.
     * @private
     **/
    onDrop:function(dropTarget)
    {
        // Activate a "smart insert" If the user drop this figure on connection
        //
        if(dropTarget instanceof draw2d.Connection){
            var oldSource = dropTarget.getSource();
            dropTarget.setSource(this.getOutputPort(0));
        
            var additionalConnection = new draw2d.Connection();
            this.getCanvas().addFigure(additionalConnection);
            additionalConnection.setSource(oldSource);
            additionalConnection.setTarget(this.getInputPort(0));
        }
    },

      /**
     * @method
     * called by the framework if the figure should show the contextmenu.</br>
     * The strategy to show the context menu depends on the plattform. Either loooong press or
     * right click with the mouse.
     * 
     * @param {Number} x the x-coordinate to show the menu
     * @param {Number} y the y-coordinate to show the menu
     * @since 1.1.0
     */
    onContextMenu:function(x,y){
        $.contextMenu({
            selector: 'body', 
            events:
            {  
                hide:function(){ $.contextMenu( 'destroy' ); }
            },
            callback: $.proxy(function(key, options) 
            {
               switch(key){
               case "red":
                   this.setBackgroundColor("ff0000");
                   break;
               case "green":
                   this.setBackgroundColor("00ff00");
                   break;
               case "blue":
                   this.setBackgroundColor("0000ff");
                   break;
               case "delete":
                   // without undo/redo support
              //     this.getCanvas().removeFigure(this);
                   
                   // with undo/redo support
                   var cmd = new draw2d.command.CommandDelete(this);
                   this.getCanvas().getCommandStack().execute(cmd);
               default:
                   break;
               }
            
            },this),
            x:x,
            y:y,
            items: 
            {
                "red":    {name: "Red"},
                "green":  {name: "Green"},
                "blue":   {name: "Blue"},
                "sep1":   "---------",
                "delete": {name: "Delete"},
            }
        });
}
});