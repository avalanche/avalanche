workflow.Toolbar = Class.extend({

    init:function (elementId, view) {
        this.html = $("#" + elementId);
        this.view = view;

        // register this class as event listener for the canvas
        // CommandStack. This is required to update the state of
        // the Undo/Redo Buttons.
        //
        view.getCommandStack().addEventListener(this);

        // Register a Selection listener for the state hnadling
        // of the Delete Button
        //
        view.addSelectionListener(this);

        // Inject the UNDO Button and the callbacks
        //
        this.undoButton = $('<button class="btn">Undo</button>');
        this.html.append(this.undoButton);
        this.undoButton.button().click($.proxy(function () {
            this.view.getCommandStack().undo();
        }, this)).button("option", "disabled", true);

        // Inject the REDO Button and the callback
        //
        this.redoButton = $('<button class="btn">Redo</button>');
        this.html.append(this.redoButton);
        this.redoButton.button().click($.proxy(function () {
            this.view.getCommandStack().redo();
        }, this)).button("option", "disabled", true);

        this.delimiter = $("<span class='toolbar_delimiter'>&nbsp;</span>");
        this.html.append(this.delimiter);

        // Inject the DELETE Button
        //
        this.deleteButton = $('<button class="btn">Delete</button>');
        this.html.append(this.deleteButton);
        this.deleteButton.button().click($.proxy(function () {
            var node = this.view.getCurrentSelection();
            var command = new draw2d.command.CommandDelete(node);
            this.view.getCommandStack().execute(command);
        }, this)).button("option", "disabled", true);


        // Inject the SAVE Button and the callbacks
        //
        this.saveButton = $('<button class="btn">Save</button>');
        this.html.append(this.saveButton);
        this.saveButton.button().click($.proxy(function () {
            var writer = new draw2d.io.json.Writer();
            //alert(JSON.stringify(writer.marshal(this.view)));

            //$("#blockmap").text(JSON.stringify(writer.marshal(this.view), null, 2));
        }, this)).button("option", "disabled", false);

        // Inject the EXPORT Button and the callbacks
        //
        this.exportButton = $('<button class="btn">Export</button>');
        this.html.append(this.exportButton);
        this.exportButton.button().click($.proxy(function () {
            var writer = new draw2d.io.json.Writer();
            alert(JSON.stringify(writer.marshal(this.view)));
            //$("#blockmap").text(JSON.stringify(writer.marshal(this.view), null, 2));
        }, this)).button("option", "disabled", false);
    },


    displayJSON:function () {
        var writer = new draw2d.io.json.Writer();
        $("#blockmap").text(JSON.stringify(writer.marshal(this.view), null, 2));
        $("#blockmap").hide();
    },

    /**
     * @method
     * Called if the selection in the cnavas has been changed. You must register this
     * class on the canvas to receive this event.
     *
     * @param {draw2d.Figure} figure
     */
    onSelectionChanged:function (figure) {
        this.deleteButton.button("option", "disabled", figure === null);
    },

    /**
     * @method
     * Sent when an event occurs on the command stack. draw2d.command.CommandStackEvent.getDetail()
     * can be used to identify the type of event which has occurred.
     *
     * @template
     *
     * @param {draw2d.command.CommandStackEvent} event
     **/
    stackChanged:function (event) {
        this.undoButton.button("option", "disabled", !event.getStack().canUndo());
        this.redoButton.button("option", "disabled", !event.getStack().canRedo());
        if (event.isPostChangeEvent()) {
            this.displayJSON(this);
        }
        //document.getElementById('blockmapval').value = this.displayJSON(this);
    }

});