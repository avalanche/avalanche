import requests
import json
from collections import defaultdict, Mapping, Iterable


def autovivify(levels=1, final=dict):
    """Similar to perl's autovivificaiton feature
    returns last value in dict with a parameter for the return type of final level
    usage: counting the number of occurrences of words said by people by year, month, and day in a chat room
    words = autovivify(5, int)
    words["sam"][2012][5][25]["hello"] += 1
    words["sue"][2012][5][24]["today"] += 1
    """
    return (defaultdict(final) if levels < 2 else
            defaultdict(lambda: autovivify(levels - 1, final)))


class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""

    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


def apiGet(model, host='http://127.0.0.1:5000/', user='avalanche', pw='aquatestla123', type='dict'):
    """This function returns the parameters for the frontend with an api call to an external url

   :param model: The api url path to use (ie block_list or route_list etc).
   :type model: str.
   :param type: the return data type wanted (either dict or json).
   :type type: str.
   :returns:  dict or json -- the return data.
   :raises: Exception

   """
    try:
        r = requests.get('%s%s' % (host, model), auth=(
            user, pw))
        if type == 'dict':
            response = r.json
        elif type == 'json':
            response = json.dumps(r.json)
        else:
            response = None
    except Exception, e:
        if type == 'dict':
            response = {'blocks': {'None': {'params': {'Null':
                       {'default': 'None', 'type': 'None', 'desc': str(e)}}}}}
        elif type == 'json':
            response = '[{"blocks":{"None":{"params":{"Null":{"default":"None","type":"None","desc":str(e)}}}}}]'
        else:
            response = None
    return response


def convert(data):
    if isinstance(data, unicode):
        return str(data)
    elif isinstance(data, Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, Iterable):
        return type(data)(map(convert, data))
    else:
        return data


def blockParser(data, rtype):
    """Returns block classnames as rtype format

    """
    rawData = AutoVivification(data)
    if rtype == 'list':
        result = list(rawData['blocks'].keys())
    elif rtype == 'tuple':
        blocks = {}
        for b in rawData['blocks']:
            blocks[b] = b
        result = tuple(blocks.items())
    elif rtype == 'dict':
        result = rawData['blocks']
    else:
        result = None
    return result


def parameterParser(data, block, rtype):
    rawData = AutoVivification(data)
    paramdict = rawData['blocks'][block]['params']
    if rtype == 'list':
        result = list(paramdict.items())
    elif rtype == 'tuple':
        params = {}
        plist = paramdict.keys()
        x = 0
        for p in plist:
            val = str(unicode.decode(p))
            params[val] = val
            x += 1
        result = tuple(params.items())
    elif rtype == 'dict':
        result = paramdict
    else:
        result = None
    return result
