# forms.py
from django.forms import ModelForm
from django import forms
from workbench.parse import apiGet, blockParser
from workbench.models import WorkFlow, Block


class BlockAdminForm(ModelForm):
    data = apiGet('block_list')
    blocks = blockParser(data, 'tuple')

    name = forms.CharField(max_length=200)
    params = forms.ChoiceField(choices=blocks,
                               widget=forms.Select(
                               attrs={
                               'onChange': '$("#blockparams").empty(); Dajaxice.workbench.updateadmincombo(Dajax.process, {"option":this.value})'}))

    class Meta:
        model = Block


class WorkFlowAdminForm(ModelForm):
    class Meta:
        model = WorkFlow


class BlockForm(ModelForm):
    class Meta:
        model = Block


class WorkFlowForm(ModelForm):
    class Meta:
        model = WorkFlow
