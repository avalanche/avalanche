from datasets.models import DataSet
from workbench.models import WorkFlow, Block, Route
from django.contrib import admin, messages
from django.core import serializers
from account.models import Account, AccountDeletion, SignupCode
from workbench.forms import BlockAdminForm, WorkFlowAdminForm


def serialize(modeladmin, request, queryset):
    messages.info(request, serializers.serialize(
        'json', queryset, indent=4, excludes="modified,created,slug,name"))


class BlockInline(admin.TabularInline):
    model = Block
    fk_name = "workflow"


class RouteInline(admin.TabularInline):
    model = Route
    extra = 1
    fk_name = "workflow"


class DataSetInline(admin.TabularInline):
    model = DataSet
    extra = 0
    max_num = 1


class BlockAdmin(admin.ModelAdmin):
    form = BlockAdminForm
    readonly_fields = ('uuid', 'paramdata')
    inlines = (DataSetInline,)
    list_filter = ('modified', 'created')
    exclude = ["paramdata"]
    actions = [serialize]

    def save_model(self, request, obj, form, change):
        pdata = str(request.POST['paramdata'])
        print(pdata)
        obj.paramdata = pdata
        obj.save()


class RouteAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
    inlines=(BlockInline,)
    list_filter = ('modified', 'created')
    actions = [serialize]


class WorkFlowAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid', 'created_by')
    exclude = ('created_by', )
    form = WorkFlowAdminForm
    inlines = (RouteInline,)
    list_filter = ('modified', 'created')
    actions = [serialize]

    def queryset(self, request):
        if request.user.is_superuser:
            return WorkFlow.objects.all()
        return WorkFlow.objects.filter(created_by=request.user)

    def has_change_permission(self, request, obj=None):
        has_class_permission = super(
            WorkFlowAdmin, self).has_change_permission(request, obj)
        if not has_class_permission:
            return False
        if obj is not None and not request.user.is_superuser and request.user.id != obj.created_by.id:
            return False
        return True

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        obj.save()

#remove unneeded things
admin.site.unregister(Account)
admin.site.unregister(SignupCode)
admin.site.unregister(AccountDeletion)

admin.site.register(WorkFlow, WorkFlowAdmin)
admin.site.register(Block, BlockAdmin)
