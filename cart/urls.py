from django.conf import settings
from django.conf.urls import patterns, url
from django.conf.urls.static import static
from cart.views import CartView

urlpatterns = patterns('cart.views',
    url(r'^$', CartView.as_view(), name="cart_home"),
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
