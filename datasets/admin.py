from datasets.models import DataSet
from django.contrib import admin, messages
from django.core import serializers


def serialize(modeladmin, request, queryset):
    messages.info(request, serializers.serialize('json', queryset, indent=4, excludes="modified,created,slug,name"))


class DataSetAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
    list_display = ['data', ]
    list_filter = ('modified', 'created')
    actions = [serialize]

admin.site.register(DataSet, DataSetAdmin)
