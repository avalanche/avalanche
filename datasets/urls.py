from django.conf import settings
from django.conf.urls import patterns, url
from django.conf.urls.static import static
#from datasets.views import DataSetListView, DataSetEdit, DataSetDelete

SLUG = '(?P<slug>[\w\d-]+)'

urlpatterns = patterns('datasets.views',
                       url(r'^$', 'list', name="list"),
                       #url(r'^' + SLUG + '/$', DataSetEdit.as_view(), name='dataset_edit'),
                       #url(r'^' + SLUG + '/delete/$', DataSetDelete.as_view(), name='dataset_delete'),
                       )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
