from django.db import models

from django_extensions.db.models import TimeStampedModel
from django_extensions.db.fields import UUIDField
from autoslug import AutoSlugField
from workbench.models import Block

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class DataSet(TimeStampedModel):
    data = models.FileField(upload_to="data")
    uuid = UUIDField(version=4, unique=True)
    block = models.ForeignKey(Block)
    slug = AutoSlugField(populate_from='data')

    def __unicode__(self):
        return self.data.name

    def save(self, **kwargs):
        super(DataSet, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('data_edit', kwargs={'pk': self.pk})
