# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from datasets.models import DataSet
from datasets.forms import DataSetForm


def list(request):
    # Handle file upload
    if request.method == 'POST':
        form = DataSetForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = DataSet(data=request.FILES['data'])
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('datasets.views.list'))
    else:
        form = DataSetForm()  # A empty, unbound form

    # Load documents for the list page
    dataset_list = DataSet.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'dataset/dataset_list.html',
        {'dataset_list': dataset_list, 'form': form},
        context_instance=RequestContext(request)
    )
