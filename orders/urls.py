from django.conf import settings
from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from tastypie.api import Api
from orders.api import OrderItemResource, OrderResource

#setup api resources
v1_api = Api(api_name='v1')
v1_api.register(OrderItemResource())
v1_api.register(OrderResource())


urlpatterns = patterns('orders.views',
                       url(r'^api/', include(v1_api.urls)),
                       )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#urlpatterns += staticfiles_urlpatterns()
