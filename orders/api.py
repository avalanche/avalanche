__author__ = 'hur1can3'

from tastypie.resources import ModelResource
from tastypie.serializers import Serializer
from tastypie import fields
from orders.models import Order, OrderItem
from beers.api import BeerResource


class OrderItemResource(ModelResource):
    productID = fields.ForeignKey(BeerResource, 'productID', full=True)

    class Meta:
        queryset = OrderItem.objects.all()
        resource_name = 'orderitem'
        #excludes = ['created', 'modified', 'slug']
        serializer = Serializer()
        include_resource_uri = False
        allowed_methods = ['get', 'post']


class OrderResource(ModelResource):
    items = fields.ToManyField(OrderItemResource, attribute=lambda bundle: OrderItem.objects.filter(order=bundle.obj),
                               full=True)

    class Meta:
        queryset = Order.objects.all()
        resource_name = 'order'
        #excludes = ['blockmap', 'created', 'modified', 'slug']
        include_resource_uri = False
        serializer = Serializer()
        allowed_methods = ['get', 'post']
