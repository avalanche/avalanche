
from orders.models import Order, OrderItem
from django.contrib import admin


def resave(modeladmin, request, queryset):
    for o in Order.objects.all():
        o.save()


class OrderItemAdminInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    readonly_fields = ('total',)


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemAdminInline, ]
    actions = [resave]

admin.site.register(Order, OrderAdmin)
