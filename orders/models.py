from django.db import models
from beers.models import Beer
from django.contrib.auth.models import User


class Order(models.Model):
    customerID = models.OneToOneField(User)
    date_time = models.DateTimeField()

    class Meta:
        ordering = ["date_time"]

    def __unicode__(self):
        return str(self.date_time) + " " + str(self.customerID.username)

    def save(self, **kwargs):
        super(Order, self).save(**kwargs)


class OrderItem(models.Model):
    order = models.ForeignKey(Order)
    productID = models.ForeignKey(Beer)
    amount = models.PositiveSmallIntegerField(default=1)
    total = models.CharField(max_length=200)

    def __unicode__(self):
        return self.productID.name

    def save(self, *args, **kwargs):
        b = Beer.objects.get(pk=self.productID.pk)
        self.total = b.price + self.amount
        super(OrderItem, self).save(*args, **kwargs)
