# -*- coding: utf-8 -*-
from django.views.generic import ListView, TemplateView

from results.models import Result
from results import mongo

import pickle


# class ResultListView(TemplateView):
#     model = Result
#     template_name = "results/results_list.html"

#     def get_context_data(self, **kwargs):
#         context = super(ResultListView, self).get_context_data(**kwargs)
#         context['result_list'] = Result.objects.get(pk=self.kwargs.get('pk', None))

class ResultListView(ListView):
    template_name = "results/result_list.html"
    model = Result

    def get_queryset(self):
        if self.request.user.is_authenticated():
            return Result.objects.all()


class ResultView(TemplateView):
    model = Result
    template_name = "results/result_view.html"

    def get_context_data(self, **kwargs):
        context = super(ResultView, self).get_context_data(**kwargs)
        object = Result.objects.get(pk=self.kwargs.get('pk', None))
        context['result'] = object 
        #print(object.uuid)
        context['data'] =  mongo.parseResult(object.uuid)
        return context
