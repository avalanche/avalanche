from django.db import models
from django_extensions.db.models import TimeStampedModel
from django_extensions.db.fields import UUIDField

from django.core.urlresolvers import reverse


class Result(TimeStampedModel):
    # data = jsonfield.JSONField()
    uuid = UUIDField(version=4, unique=True)
    rtype = models.CharField(max_length=200)

    def __unicode__(self):
        return self.uuid

    def save(self, **kwargs):
        super(Result, self).save(**kwargs)

    @models.permalink
    def get_absolute_url(self):
        return reverse('result_list', kwargs={'pk': self.pk})
