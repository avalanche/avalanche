from results.models import Result
from django.contrib import admin


class ResultAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid',)
    list_filter = ('modified', 'created' )

admin.site.register(Result, ResultAdmin)
