from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from results.views import ResultListView, ResultView
from tastypie.api import Api
from results.api import ResultResource

#setup api resources
v1_api = Api(api_name='v1')
v1_api.register(ResultResource())

urlpatterns = patterns('',
                       url(r'^api/', include(v1_api.urls)),
                       )

urlpatterns += patterns('results.views',
                        url(r'^$', ResultListView.as_view(), name="result_list"),
                        url(r'^(?P<pk>\d+)/$', ResultView.as_view(), name="result_view"),
                        url(r'^page/(?P<page>\d+)/$', ResultListView.as_view(),
                            name='result_list_paginated')
                        )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
