#import gridfs
import os
import pickle
import urlparse
from pymongo import Connection
from pymongo.collection import Collection

MONGO_URL = os.environ.get('MONGOHQ_URL')

connection, db = None, None

if MONGO_URL:
    connection = Connection(MONGO_URL)
    db = connection[urlparse(MONGO_URL).path[1:]]
else:
    connection = Connection('localhost', 27017)
    db = connection['avalanche']


def parseResult(resultid):
    print(resultid)
    res = db['blocks_meta']
    results = res.find_one({'_id': str(resultid)})
    for i in results:
        if i == 'result':
            #print "Result value: %s" % (pickle.loads(results[i]))
            return pickle.loads(results[i])
            
