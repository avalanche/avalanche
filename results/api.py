__author__ = 'hur1can3'

from tastypie.resources import ModelResource
from tastypie.authentication import Authentication, BasicAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from results.models import Result


class ResultResource(ModelResource):
    class Meta:
        queryset = Result.objects.all()
        resource_name = 'result'
        allowed_methods = ['get', 'post', 'put', 'delete']
        excludes = ['created', 'modified', 'id']
        include_resource_uri = False
        authentication = BasicAuthentication()
        authorization = DjangoAuthorization()

    # def obj_create(self, bundle, request=None, **kwargs):
    #     print(request.data)
    #     #return request
    #     return super(ResultResource, self).obj_create(bundle, request)

    #def apply_authorization_limits(self, request, object_list):
    #    return object_list.filter(user=request.user)
